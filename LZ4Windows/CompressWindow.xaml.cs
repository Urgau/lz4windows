﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LZ4Windows
{
    /// <summary>
    /// Logique d'interaction pour CompressWindow.xaml
    /// </summary>
    public partial class CompressWindow : Window
    {
        WaitingWindow ww = new WaitingWindow();
        LZ4Compressor lz4 = new LZ4Compressor();

        public CompressWindow(string pathToCompress)
        {
            InitializeComponent();

            LabelPath.Content = pathToCompress;
            lz4.FinishCompress += new LZ4Compressor.DelegateFinishCompress(LZ4Finish);
            lz4.Error += new LZ4Compressor.DelegateError(LZ4Error);
        }

        private void ButtonCompress_Click(object sender, RoutedEventArgs e)
        {
            ww.ShowWaiting();
            try
            {
                string pathNotCompress = (string)LabelPath.Content;
                string pathCompress = (string)LabelPath.Content + ".lz4";

                lz4.Compress(pathNotCompress, pathCompress);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenu !\n" + ex.ToString());
            }
        }

        private void ButtonCompressTo_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.Description = "FZ4Windows - Choose a folder";
            fbd.RootFolder = Environment.SpecialFolder.DesktopDirectory;

            System.Windows.Forms.DialogResult result = fbd.ShowDialog();
            if (result ==  System.Windows.Forms.DialogResult.OK)
            {
                ww.ShowWaiting();
                try
                {
                    string pathNotCompress = (string)LabelPath.Content;
                    string pathCompress = fbd.SelectedPath + "\\" + Path.GetFileName((string)LabelPath.Content) + ".lz4";

                    lz4.Compress(pathNotCompress, pathCompress);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Une erreur est survenu !\n" + ex.ToString());
                }
            }
        }

        private void ButtonCompressToEmail_Click(object sender, RoutedEventArgs e)
        {
            //Not work
        }

        private void LZ4Finish(double r)
        {
            ww.CloseWaiting();
            if (r >= 54) { MessageBox.Show("Compress finish !\nRatio : " + r + "%"); }
            else { MessageBox.Show("Compress finish !\nOmg ratio : " + r + "% !!!"); }
            Close();
        }

        private void LZ4Error(Exception ex)
        {
            ww.CloseWaiting();
            MessageBox.Show("Une erreur est survenu !\n" + ex.ToString());
            Close();
        }
    }
}
