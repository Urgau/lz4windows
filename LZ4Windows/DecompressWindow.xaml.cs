﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LZ4Windows
{
    /// <summary>
    /// Logique d'interaction pour DecompressWindow.xaml
    /// </summary>
    public partial class DecompressWindow : Window
    {
        WaitingWindow ww = new WaitingWindow();
        LZ4Compressor lz4 = new LZ4Compressor();

        public DecompressWindow(string pathToDecompress)
        {
            InitializeComponent();

            LabelPath.Content = pathToDecompress;
            lz4.FinishDecompress += new LZ4Compressor.DelegateFinishDecompress(LZ4Finish);
            lz4.Error += new LZ4Compressor.DelegateError(LZ4Error);
        }

        private void ButtonDecompress_Click(object sender, RoutedEventArgs e)
        {
            ww.ShowWaiting();
            try
            {
                string path = (string)LabelPath.Content;

                lz4.Decompress(path, path.Remove(path.Length - 4, 4));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenu !\n" + ex.ToString());
            }
        }

        private void ButtonDecompressTo_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.Description = "FZ4Windows - Choose a folder";
            fbd.RootFolder = Environment.SpecialFolder.DesktopDirectory;

            System.Windows.Forms.DialogResult result = fbd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                ww.ShowWaiting();
                try
                {
                    string path = (string)LabelPath.Content;
                    string filename = Path.GetFileName(path);

                    lz4.Decompress((string)LabelPath.Content, fbd.SelectedPath + "\\" + filename.Remove(filename.Length - 4, 4));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Une erreur est survenu !\n" + ex.ToString());
                }
            }
        }

        private void LZ4Finish()
        {
            ww.CloseWaiting();
            MessageBox.Show("Decompress finish !");
            Close();
        }

        private void LZ4Error(Exception ex)
        {
            ww.CloseWaiting();
            MessageBox.Show("Une erreur est survenu !\n" + ex.ToString());
            Close();
        }
    }
}
