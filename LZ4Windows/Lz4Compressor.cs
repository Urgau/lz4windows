﻿using LZ4;
using System;
using System.IO;
using System.Threading.Tasks;

namespace LZ4Windows
{
    class LZ4Compressor
    {
        public delegate void DelegateFinishCompress(double ratio);
        public event DelegateFinishCompress FinishCompress;

        public delegate void DelegateFinishDecompress();
        public event DelegateFinishDecompress FinishDecompress;

        public delegate void DelegateError(Exception ex);
        public event DelegateError Error;

        public async void Compress(string pathNotCompress, string pathCompress, bool highCompress = false)
        {
            if (!File.Exists(pathNotCompress)) { throw new FileNotFoundException(pathNotCompress + " is not found !"); }

            Exception exTask = null;
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    using (FileStream sFileNotCompress = new FileStream(pathNotCompress, FileMode.Open, FileAccess.Read))
                    {
                        using (FileStream sFileCompress = new FileStream(pathCompress, FileMode.Create))
                        {
                            Compress(sFileCompress, sFileNotCompress, highCompress);
                        }
                    }
                }
                catch (Exception ex)
                {
                    exTask = ex;
                }
            });

            if (exTask == null)
            {
                long ocNotCompress = new FileInfo(pathNotCompress).Length;
                long ocCompress = new FileInfo(pathCompress).Length;
                double ratio = 100 - (100 * ocCompress / ocNotCompress);

                FinishCompress(ratio);
            }
            else { Error(exTask); }
        }

        private void Compress(Stream sCompress, Stream sNotCompress, bool highCompress = false)
        {
            LZ4Stream s = new LZ4Stream(sCompress, LZ4StreamMode.Compress, highCompress);
            sNotCompress.CopyTo(s);
            s.Flush();
        }

        public async void Decompress(string pathCompress, string pathDecompress)
        {
            if (!File.Exists(pathCompress)) { throw new FileNotFoundException(pathCompress + " is not found !"); }

            Exception exTask = null;
            await Task.Factory.StartNew(() =>
            {
                try
                {
                    using (FileStream sFileCompress = new FileStream(pathCompress, FileMode.Open, FileAccess.Read))
                    {
                        using (FileStream sFileDecompress = new FileStream(pathDecompress, FileMode.Create))
                        {
                            Decompress(sFileCompress, sFileDecompress);
                        }
                    }
                }
                catch (Exception ex)
                {
                    exTask = ex;
                }
            });

            if (exTask == null) { FinishDecompress(); }
            else { Error(exTask); }
        }

        private void Decompress(Stream sCompress, Stream sDecompress)
        {
            LZ4Stream s = new LZ4Stream(sCompress, LZ4StreamMode.Decompress);
            s.CopyTo(sDecompress);
            s.Flush();
        }
    }
}
