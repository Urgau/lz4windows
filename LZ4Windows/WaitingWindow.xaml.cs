﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LZ4Windows
{
    /// <summary>
    /// Logique d'interaction pour WaitingWindow.xaml
    /// </summary>
    public partial class WaitingWindow : Window
    {
        public WaitingWindow()
        {
            InitializeComponent();
        }

        bool isForce = false;
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!isForce) { e.Cancel = true; }
        }

        public void ShowWaiting()
        {
            Show();
            Mouse.OverrideCursor = Cursors.Wait;
        }

        public void CloseWaiting()
        {
            Mouse.OverrideCursor = Cursors.Arrow;
            isForce = true;
            Close();
        }
    }
}
