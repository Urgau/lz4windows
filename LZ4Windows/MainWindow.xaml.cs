﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LZ4Windows
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FileAssociation fa = new FileAssociation(".lz4", "LZ4Windows");
        MenuExplorer me = new MenuExplorer("LZ4Windows");

        public MainWindow()
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length == 3)
            {
                if (File.Exists(args[2]))
                {
                    if (args[1] == "-compress")
                    {
                        CompressWindow cw = new CompressWindow(args[2]);
                        cw.ShowDialog();
                        Environment.Exit(0);
                    }
                    else if (args[1] == "-decompress")
                    {
                        DecompressWindow dw = new DecompressWindow(args[2]);
                        dw.ShowDialog();
                        Environment.Exit(0);
                    }
                }
            }

            InitializeComponent();
            MenuItemAssociationLZ4File.IsChecked = fa.IsAssociate();
            MenuItemWindowsMenuCompress.IsChecked = me.Exist();
        }

        private void MenuItemLZ4_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://cyan4973.github.io/lz4/");
        }

        private void MenuItemWebsite_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://urgau.bitbucket.org/lz4windows/");
        }

        private void MenuItemLicense_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://urgau.bitbucket.org/lz4windows/license.html");
        }

        private void ButtonCompress_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "LZ4Windows - Select file to compress";
            ofd.Filter = "All File|*.*";
            ofd.ShowDialog();
            if (File.Exists(ofd.FileName))
            {
                this.Hide();
                CompressWindow cw = new CompressWindow(ofd.FileName);
                cw.ShowDialog();
                this.Show();
            }
        }

        private void ButtonDecompress_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "LZ4Windows - Select file to decompress";
            ofd.Filter = "LZ4 File|*.lz4";
            ofd.ShowDialog();
            if (File.Exists(ofd.FileName))
            {
                this.Hide();
                DecompressWindow cw = new DecompressWindow(ofd.FileName);
                cw.ShowDialog();
                this.Show();
            }
        }
        
        private void MenuItemAssociationLZ4File_Click(object sender, RoutedEventArgs e)
        {
            if (fa.IsAssociate())
            {
                fa.Deassociate();
            }
            else
            {
                fa.Associate("Open LZ4 File with LZ4Windows", System.Reflection.Assembly.GetExecutingAssembly().Location + " -decompress \"%1\"");
            }
            MenuItemAssociationLZ4File.IsChecked = fa.IsAssociate();
        }

        private void MenuItemWindowsMenuCompress_Click(object sender, RoutedEventArgs e)
        {
            if (me.Exist())
            {
                me.Remove();
            }
            else
            {
                me.Create("Compress with LZ4Windows", System.Reflection.Assembly.GetExecutingAssembly().Location + " -compress \"%1\"");
            }

            MenuItemWindowsMenuCompress.IsChecked = me.Exist();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
