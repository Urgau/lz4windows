﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZ4Windows
{
    class FileAssociation
    {
        public string Extension { get; private set; }
        public string AppName { get; private set; }

        public FileAssociation(string extension, string appName)
        {
            Extension = extension; AppName = appName;
        }

        public void Associate(string OpenWith, string Path)
        {
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\" + Extension, "", AppName, RegistryValueKind.String);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\" + AppName + "\\shell\\" + "OpenWith" + AppName, "", OpenWith, RegistryValueKind.String);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\" + AppName + "\\shell\\" + "OpenWith" + AppName + "\\command", "", Path, RegistryValueKind.String);
        }

        public void Deassociate()
        {
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\" + Extension, "", "", RegistryValueKind.String);
            RegistryKey regClasses = Registry.CurrentUser.OpenSubKey("Software\\Classes", true);
            regClasses.DeleteSubKeyTree(AppName);
        }

        public bool IsAssociate()
        {
            RegistryKey reg = Registry.CurrentUser.OpenSubKey(@"Software\Classes");
            if (reg != null)
            {
                RegistryKey regAppName = reg.OpenSubKey(AppName);
                if (regAppName == null)
                {
                    return false;
                }
                else
                {
                    RegistryKey regExtension = reg.OpenSubKey(Extension);
                    if (regExtension == null)
                    {
                        return false;
                    }
                    else
                    {
                        if (((string)regExtension.GetValue("")) != AppName)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                throw new Exception("HKEY_CURRENT_USER\\Software\\Classes\\ not exist !");
            }
        }
    }

    class MenuExplorer
    {
        public string AppName { get; private set; }

        public MenuExplorer(string appName)
        {
            AppName = appName;
        }

        public void Create(string Text, string Path)
        {
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\*\\shell\\" + AppName, "", Text, RegistryValueKind.String);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Classes\\*\\shell\\" +  AppName + "\\command", "", Path, RegistryValueKind.String);
        }

        public void Remove()
        {
            RegistryKey regEtoile = Registry.CurrentUser.OpenSubKey("Software\\Classes\\*", true);
            RegistryKey regShell = regEtoile.OpenSubKey("shell", true);
            if (regShell != null)
            {
                regShell.DeleteSubKeyTree(AppName);
            }
        }

        public bool Exist()
        {
            RegistryKey regEtoile = Registry.CurrentUser.OpenSubKey("Software\\Classes\\*", true);
            RegistryKey regShell = regEtoile.OpenSubKey("shell", true);
            if (regShell != null)
            {
                RegistryKey regAppName = regShell.OpenSubKey(AppName);
                if (regAppName == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
